drop table COMPOSER;
drop table FAIRE_PARTIE;
drop table COLIS;
drop table COMMANDE;
drop table Client; 
-- Client pointe vers Point_de_dist donc on doit commencer par drop Client 
drop table POINT_DE_DISTRIBUTION;
drop table ARTICLE;


CREATE TABLE POINT_DE_DISTRIBUTION (
    refPointDist number (5),
    adressePointDeDistrib varchar2 (100),
    tel number (10),
    CONSTRAINT pk_pointDist PRIMARY KEY (refPointDist)
);

CREATE TABLE Client (
    codeCl number (5),
    nomCl varchar2 (20),
    prenomCl varchar2 (20),
    adresseCl varchar2 (100),
    emailCl varchar2 (40),
    refPointDist number(5) NOT NULL,
    CONSTRAINT pk_client PRIMARY KEY (codeCl),
    CONSTRAINT fk_client_pointdist FOREIGN KEY (refPointDist) REFERENCES POINT_DE_DISTRIBUTION(refPointDist)
);
-- refPointDist doit-être unique dans la table point Distribution pour faire une clés étrangère vers cette attribut 

CREATE TABLE COMMANDE (
    referenceCom number (5),
    dateCom date,
    codeCl number (5),
    CONSTRAINT pk_reference_com PRIMARY KEY (referenceCom),
    CONSTRAINT fk_codeCl FOREIGN KEY (codeCl) REFERENCES Client(codeCl)
);

CREATE TABLE ARTICLE (
    referenceArt number (5),
    designationArt varchar2 (40),
    prixHT number (10),
    TVA number (2),
    quantiteStockArtk varchar2 (10),
    CONSTRAINT pk_ref_art PRIMARY KEY (referenceArt)
);
CREATE TABLE FAIRE_PARTIE (
    referenceArt number (5),
    referenceCom number (5),
    quantiteCommander number (10),
    CONSTRAINT pk_fairePartie PRIMARY KEY (referenceArt,referenceCom),
    CONSTRAINT fk_referenceArt FOREIGN KEY (referenceArt) REFERENCES ARTICLE (referenceArt),
    CONSTRAINT fk_referenceCom FOREIGN KEY (referenceCom) REFERENCES COMMANDE (referenceCom)
);

CREATE TABLE COLIS (
    referenceCom number (5),
    numerosColis number (5),
    indicateurRetraitColis number (1),
    CONSTRAINT pk_Colis PRIMARY KEY (numerosColis,referenceCom),
    CONSTRAINT fk_commande FOREIGN KEY (referenceCom) REFERENCES COMMANDE(referenceCom)
);

CREATE TABLE COMPOSER (
    referenceArt number (5),
    referenceCom number (5),
    numerosColis number (5),
    quantitexpedie number (5),
    quatiteaccepte number (5),
    CONSTRAINT pk_composer PRIMARY KEY (referenceArt,referenceCom,numerosColis),
    CONSTRAINT fk_ref_art FOREIGN KEY (referenceArt) REFERENCES ARTICLE (referenceArt),
    CONSTRAINT fk_ref_com FOREIGN KEY (referenceCom) REFERENCES COMMANDE (referenceCom),
    CONSTRAINT fk_num_coli FOREIGN KEY (numerosColis) REFERENCES COLIS (numerosColis)
);
-- Impossible de bettre des bool donc utiliser number(1)
